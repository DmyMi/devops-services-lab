package cloud.dmytrominochkin.serviceslab

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.ServerResponse

@SpringBootApplication
class ServicesLabApplication {
    @Bean
    fun imgRouter(): RouterFunction<ServerResponse> =
        RouterFunctions.resources("/static/**", ClassPathResource("static/"))

    @Bean
    fun corsConfigurer(): WebFluxConfigurer = object : WebFluxConfigurer {
        override fun addCorsMappings(registry: CorsRegistry) {
            registry
                .addMapping("/*")
                .allowedMethods("GET", "POST")
                .allowedOrigins("*")
        }
    }
}

fun main(args: Array<String>) {
    runApplication<ServicesLabApplication>(*args)
}
