package cloud.dmytrominochkin.serviceslab

import kotlinx.serialization.Serializable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.context.ApplicationContext
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

var n = 0

@RestController
class LabController @Autowired constructor(private val context: ApplicationContext) {

    @GetMapping("/")
    fun main(): String {
        n += 1
        return """
                <h1>Congratulations, you are visitor $n</h1>
                <br>
                <img src="/static/cat.gif" width="1280" height="720"/>
            """.trimIndent()
    }

    @GetMapping("/crash")
    fun getCrash(): String =
        "You can either make a <i>POST</i> request to this endpoint with the json payload <b>{\"crash\":true}</b> (if you are cool) or you can use a command to manually kill the process"

    @PostMapping("/crash")
    fun doCrash(@RequestBody req: Crash): String {
        req.crash?.let { if (it) SpringApplication.exit(context, { 1 }) }

        return "Make a correct request"
    }
}

@Serializable
data class Crash(var crash: Boolean? = false)